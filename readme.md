An implement of software simulation of a cache memory subsystem. "cache" will be a software structure/class that will contain information similar (but on a smaller and more simplified scale) to what would be in a real cache. "main memory" will be a 2K array, an array of integers or shorts (16-bits).

How to format the input file:
------------------------------
- For Read use r, followed by the requested address at the next line.
  Example for read: read address 7ae
  r
  7ae
  
- For Write use w, followed by the address to write to on the next line, then the data to be written on the 3rd line.
 Example for write: write ff(data) to address 7ae
                   w
                   7ae
                   ff
                   
- For Display use d, and it will print the current status of the cache as 
  Slot#  |  Valid  |  Tag  |  DirtyBit  |  Data
  
  
Example:
r
7ae
w
7ae
ff

Please note: if the you input file does not follow the correct format, the program will run into errors 
