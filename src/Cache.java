public class Cache {

    //Inner class (Node) represents a slot/block of data
    private static class Slot{
        private byte validFlag;
        private byte dirtyBit;
        private short tag;
        private short[] data;

        public Slot(){
            validFlag = 0;
            dirtyBit = 0;
            tag = 0;
            data = new short[16];
            dataInit();
        }
        //initialize the block data
        private void dataInit(){
            for (int i=0; i<16;i++){
                data[i]=0;
            }
        }
        public String toString(){
            String report = validFlag +"\t " + tag + "\t " + dirtyBit + "\t ";
            for (short num:data) {
                report += Integer.toHexString(num) + " ";
            }
            return report;
        }
    }

    private Slot[] cache;

    public Cache(){
        cache = new Slot[16];
        initCache();
    }

    //initialize cache
    private void initCache(){
        for (int i=0; i < 16; i++){
            cache[i] = new Slot();
        }
    }

    public byte getValidFlag(int slot){
        return cache[slot].validFlag;
    }

    public byte getDirtyBit(int slot){
        return cache[slot].dirtyBit;
    }
    public short getTag(int slot){
        return cache[slot].tag;
    }

    public int getSlotData(int slotNum,int offset){
        return cache[slotNum].data[offset];
    }

    public void setSlotData(int slotNum, int offset, short data){
        cache[slotNum].data[offset] = data;
    }

    public void setDirtyBit(int slot, byte value){
        cache[slot].dirtyBit = value;
    }

    public void setValidFlag(int slot, byte value){
        cache[slot].validFlag = value;
    }

    public void setTag(int slot, short value){
        cache[slot].tag = value;
    }

    public String toString(){
        StringBuilder display = new StringBuilder("Slot Valid Tag\tDirtyBit\tData\n");
        for (int x=0; x<16; x++){
            display.append(Integer.toHexString(x)+"\t \t"+cache[x].validFlag+"\t "+ Integer.toHexString(cache[x].tag) +
                    "\t " +cache[x].dirtyBit+"\t \t");
            for (int y=0; y<16; y++){
                display.append(Integer.toHexString(cache[x].data[y])+" ");
            }
            display.append("\n");
        }
        return display.toString();
    }
}
