import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Driver {

    public static void main(String[] args) {
        Simulation sim = new Simulation();
        System.out.println("Please read README file for how to format input file.");
        try {
            BufferedReader reader = new BufferedReader(new FileReader("input.txt"));
            String line = reader.readLine().trim();

            while (line != null && !line.isEmpty()) {
                System.out.println("Instruction>> "+line);
                if ((line.length() == 1) && (line.toLowerCase().matches("r"))){

                    System.out.println(sim.read(Integer.decode("0x"+reader.readLine().trim()))+'\n');
                }
                else if ((line.length() == 1) && (line.toLowerCase().matches("w"))){
                    System.out.println(sim.write(Integer.decode("0x"+reader.readLine().trim()),(short) Integer.decode("0x"+reader.readLine().trim()).intValue())+'\n');
                }
                else if ((line.length() == 1) && (line.toLowerCase().matches("d"))){
                    System.out.println(sim.printCache()+'\n');
                }
                else {
                    System.out.println("Error: unknown instruction"+'\n');
                }
                line=reader.readLine();
            }
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
            }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
