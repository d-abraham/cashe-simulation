public class Simulation {
    private Cache cache;
    private MainMem mainMem;

    public Simulation(){
        cache = new Cache();
        mainMem = new MainMem();
    }

    //Read
    public String read(int address){
        int offset=address&0xf;
        int slotNum=(address&0xf0)>>>4;
        int tag=(address&0xf00)>>>8;
        int block = address&0xff0;
        String report="Read "+ Integer.toHexString(address)+'\n';
        if((cache.getValidFlag(slotNum)==1)&&(cache.getTag(slotNum)==tag)){    //cache hit
            report+="Cache Hit\n"+"At address " +Integer.toHexString(address)+ " there is value "+
                    Integer.toHexString(cache.getSlotData(slotNum,offset));
        }
        else{    //cache miss
            if (cache.getDirtyBit(slotNum)==1){    //Cache miss with dirtyBit = 1
                int updateMmBlock = (cache.getTag(slotNum)<<8)+(slotNum<<4);    //Starting block in MM to be updated
                report+="Cache miss - DirtyBit = 1 | Updating MM from cache, then pull in requested address from mm to cache\n";
                for (int offsetPos=0; offsetPos<16; offsetPos++){
                    //update mm from cache
                    mainMem.setMainMem(updateMmBlock+offsetPos,(short) cache.getSlotData(slotNum,offsetPos));
                    //pull in from mm to cache
                    cache.setSlotData(slotNum,offsetPos,mainMem.getMainMemAddress(block+offsetPos));
                }
                //rest dirtyBit,update validFlag and tag
                cache.setDirtyBit(slotNum,(byte)0);
                cache.setValidFlag(slotNum,(byte)1);
                cache.setTag(slotNum,(short)tag);
                report+="At address "+ Integer.toHexString(address)+" there is value "+
                        Integer.toHexString(cache.getSlotData(slotNum,offset));
            }
            else {    //Cache miss with dirtyBit = 0
                //pull in from mm to cache | update tag & validFlag
                report+= "Cache miss - DirtyBit = 0 | pull in from mm to cache\n";
                for (int offsetPos = 0; offsetPos < 16; offsetPos++) {
                    //pull in from mm to cache
                    cache.setSlotData(slotNum, offsetPos, mainMem.getMainMemAddress(block + offsetPos));
                }
                //update validFlag and tag
                cache.setValidFlag(slotNum, (byte) 1);
                cache.setTag(slotNum, (short) tag);
                report+="At address "+ Integer.toHexString(address)+" there is value "+
                        Integer.toHexString(cache.getSlotData(slotNum,offset));
                }
            }
        return report;
    }
    //Write
    public String write(int address, short data ){
        int offset=address&0xf;
        int slotNum=(address&0xf0)>>>4;
        int tag=(address&0xf00)>>>8;
        int block = address&0xff0;
        String report = "Write "+ Integer.toHexString(data)+ " to address "+ Integer.toHexString(block+offset)+'\n';
        if ((cache.getValidFlag(slotNum)==1)&&(cache.getTag(slotNum)==tag)){    //cache hit
            //write to cache only
            report+="Cache hit | modifying cache only\n";
            cache.setSlotData(slotNum,offset,data);
            cache.setDirtyBit(slotNum,(byte)1);
            report+="Value "+ Integer.toHexString(cache.getSlotData(slotNum,offset))+" has been written to address "
                    + Integer.toHexString(address);
        }
        else {    //cache miss
            //pull in the data from mm, then modify it.
            report+="Cache miss | getting data from mm and modifying it in cache\n";
            for (int offsetPos = 0; offsetPos < 16; offsetPos++) {
                // moving the block from mm to cache
                cache.setSlotData(slotNum, offsetPos, mainMem.getMainMemAddress(block + offsetPos));
            }
            cache.setSlotData(slotNum,offset,data);    //writing the data to the requested address
            //update validFlag. Update tag
            cache.setValidFlag(slotNum, (byte) 1);
            cache.setTag(slotNum, (short) tag);
            cache.setDirtyBit(slotNum,(byte)1);
            report+="Value "+ Integer.toHexString(cache.getSlotData(slotNum,offset))+" has been written to address "
                    + Integer.toHexString(address);
        }
        return report;

    }
    //Prints (display) cache
    public String printCache(){
        return "Display cache:\n"+cache.toString();
    }

    //Prints MM (for debugging)
    public String pintMM(){
        return mainMem.toSting();
    }

}
